<?php
include "inc/header.php";
?>

<div id="structure">
    <section id="presentation">
        <div class="identity">
            <div class="bg_id">
                <div class="bg_square"></div>
                <div class="bg_arrow"></div>
            </div>
            <div class="name">
                <h1 class="maintitle">christophe nehlig</h1>
            </div>

            <div class="age">
                <p>Mon âge en Javascript</p>
            </div>
        </div>

        <div class="contact">
            <p>Si mon profil vous intéresse, n'hésitez pas à me contacter sur <a
                    href="https://www.linkedin.com/">Linked'in</a></p>
        </div>

        <div class="perso_infos">
            <div class="bg_infos">
                <div class="bg_arrowup"></div>
                <div class="bg_square2"></div>
            </div>
            <div class="personality">
                <h2 class="subtitle">Personnalité</h2>
                <ul>
                    <li>Curieux</li>
                    <li>Sérieux</li>
                    <li>Créatif</li>
                    <li>Rigoureux</li>
                    <li>Polyvalent</li>
                    <li>Autonome</li>
                    <li>Résilient</li>
                </ul>
            </div>

            <div class="leisure">
                <h2 class="subtitle">Loisirs</h2>
                <ul>
                    <li>Jeux vidéos</li>
                    <li>Musculation</li>
                </ul>
            </div>

            <div class="skills">
                <h2 class="subtitle">Compétences</h2>
                <ul>
                    <li>Travail en équipe</li>
                    <li>Capacité d'adaptation</li>
                    <li>Sens de l'organisation</li>
                    <li>Français (langue maternelle)</li>
                    <li>Anglais (courant)</li>
                    <li>Capacité à coder dans plusieurs langages (HTML-CSS, PHP, SQL, Javascript)</li>
                </ul>
            </div>
        </div>
    </section>

    <section id="main_content">
            <div id="jobs">
                <h2 class="catitle">Expériences Professionnelles</h2>
                <div class="line"></div>
                <div class="job job1"></div>
                <div class="job job2"></div>
                <div class="job job3"></div>
            </div>

            <div id="schools">
                <h2 class="catitle">Formations</h2>
                <div class="line"></div>
                <div class="school school1"></div>
                <div class="school school2"></div>
                <div class="school school3"></div>
            </div>
    </section>
</div>





<script src="asset/js/main.js"></script>
<?php include "inc/footer.php"; ?>